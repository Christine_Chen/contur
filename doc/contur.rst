contur package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   contur.config
   contur.data
   contur.factories
   contur.plot
   contur.run
   contur.scan
   contur.util


Module contents
---------------

.. automodule:: contur
   :members:
   :undoc-members:
   :show-inheritance:


Executables
-----------

`contur`
~~~~~~~~
.. argparse::
   :ref: contur.run.run_analysis.get_argparser
   :prog: contur

`contur-batch`
~~~~~~~~~~~~~~
.. argparse::
   :ref: contur.run.run_batch_submit.get_argparser
   :prog: contur-batch

`contur-gridtool`
~~~~~~~~~~~~~~~~~
.. argparse::
   :ref: contur.run.run_grid_tools.get_argparser
   :prog: contur-gridtool
