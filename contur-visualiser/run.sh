# read -p "Enter contur parent directory: " contur_home
# read -p "Enter website directory: " website
# read -p "Enter dataset directory: " dataset

# read -p "Enter file to be depickled in absolute pathname: " depickle_file
# python $website/example-website/depickle.py $dataset/$depickle_file
# echo "\n"

# contur-plot $dataset/cedar/lcorpe/contur_271219_rivet3corrs/contur/run-area-TFHM/myscan_granular_lin_tail_30k_HDMYfix3/13TeV/ANA_myscan_granular_lin_tail_30k_HDMYfix3_searches/contur.map tsb mzp
# mv xcoords.txt ycoords.txt zcoords.txt /website/example-website/datapoints/

# export FLASK_APP=main.py
# export FLASK_ENV=development
# flask run

# rm contur.log
# rm excl.txt
# rm processes.txt

# -------------------------------------------------------------------------------------------------------------------------------------------------

# $1 - website dir
# $2 - dataset dir
# $3 - x
# $4 - y

rm -rf $1/conturPlot/ $1/data/ $1/contur.log $1/templates/point $1/static/point
mkdir -p $1/data

mkdir -p $2/templates/point
mkdir -p $2/static/point

python $1/depickle.py $5

# $CONTUR_ROOT/AnalysisTools/contur/bin/contur-plot $5 $3 $4
# mv xcoords.txt ycoords.txt zcoords.txt $1/data/

# Example values
# $1 - /home/shaanzie/Desktop/unix/cedar/lcorpe/contur_271219_rivet3corrs/contur/run-area-TFHM/myscan_granular_lin_tail_30k_HDMYfix3/13TeV/
# $2 - /home/shaanzie/Desktop/contur/contur-visualizer/
# $3 - mzp
# $4 - tsb
