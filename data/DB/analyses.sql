
PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;

-- Table for known/allowed beams

CREATE TABLE beams (
    id      TEXT NOT NULL UNIQUE,
    PRIMARY KEY(id) 
);
INSERT INTO beams VALUES('7TeV');
INSERT INTO beams VALUES('8TeV');
INSERT INTO beams VALUES('13TeV');

--

CREATE TABLE analysis_pool (
    pool    TEXT NOT NULL UNIQUE,
    beam    TEXT,
    description TEXT,
    PRIMARY KEY(pool),
    FOREIGN KEY(beam) REFERENCES beams(id)	
);

-- all-hadronic inclusive measurements
INSERT INTO analysis_pool VALUES('ATLAS_7_JETS','7TeV','Inclusive hadronic final states');  
INSERT INTO analysis_pool VALUES('CMS_7_JETS','7TeV','Inclusive hadronic final states');    
INSERT INTO analysis_pool VALUES('ATLAS_8_JETS','8TeV','Inclusive hadronic final states');  
INSERT INTO analysis_pool VALUES('CMS_8_JETS','8TeV','Inclusive hadronic final states');  
INSERT INTO analysis_pool VALUES('ATLAS_13_JETS','13TeV','Inclusive hadronic final states');
INSERT INTO analysis_pool VALUES('CMS_13_JETS','13TeV','Inclusive hadronic final states');

-- ALICE_2012_I944757 (charm production) good cross section but can't really use without
-- the theory prediction, since the theory uncertainties are vastly bigger than expt.

-- all-hadronic on top pole
INSERT INTO analysis_pool VALUES('ATLAS_13_TTHAD','13TeV','Fully hadronic top events');

-- inclusive isolated photons
INSERT INTO analysis_pool VALUES('ATLAS_7_GAMMA','7TeV','Inclusive (multi)photons');  
INSERT INTO analysis_pool VALUES('CMS_7_GAMMA','7TeV','Inclusive (multi)photons');      
INSERT INTO analysis_pool VALUES('ATLAS_8_GAMMA','8TeV','Inclusive (multi)photons');
INSERT INTO analysis_pool VALUES('CMS_8_GAMMA','8TeV','Inclusive (multi)photons');
INSERT INTO analysis_pool VALUES('ATLAS_13_GAMMA','13TeV','Inclusive (multi)photons');

-- dileptons below Z pole
INSERT INTO analysis_pool VALUES('ATLAS_7_LMDY','7TeV','Dileptons below the Z pole');

-- dileptons (+jets) on Z pole
INSERT INTO analysis_pool VALUES('ATLAS_7_EEJET','7TeV','e+e- at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('ATLAS_7_MMJET','7TeV','mu+mu- at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('CMS_7_LLJET','7TeV','dileptons at the Z pole, plus optional jets');  
INSERT INTO analysis_pool VALUES('CMS_7_EEJET','7TeV','e+e- at the Z pole, plus optional jets');  
INSERT INTO analysis_pool VALUES('LHCB_7_EEJET','7TeV','e+e- at the Z pole, plus optional jets');         
INSERT INTO analysis_pool VALUES('LHCB_7_MMJET','7TeV','mu+mu- at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('ATLAS_8_LLJET','8TeV','dileptons at the Z pole, plus optional jets');  
INSERT INTO analysis_pool VALUES('ATLAS_8_EEJET','8TeV','e+e- at the Z pole, plus optional jets');  
INSERT INTO analysis_pool VALUES('ATLAS_8_MMJET','8TeV','mu+mu- at the Z pole, plus optional jets');  
INSERT INTO analysis_pool VALUES('CMS_8_LLJET','8TeV','dileptons at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('ATLAS_13_EEJET','13TeV','e+e- at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('ATLAS_13_MMJET','13TeV','mu+mu- at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('CMS_13_EEJET','13TeV','dileptons at the Z pole, plus optional jets');
INSERT INTO analysis_pool VALUES('CMS_13_MMJET','13TeV','dileptons at the Z pole, plus optional jets');

-- dileptons (+optional jets) above Z pole
INSERT INTO analysis_pool VALUES('ATLAS_7_HMDY','7TeV','Dileptons above the Z pole');
INSERT INTO analysis_pool VALUES('ATLAS_8_HMDY_EL','8TeV','Dileptons above the Z pole');
INSERT INTO analysis_pool VALUES('ATLAS_8_HMDY_MU','8TeV','Dileptons above the Z pole');
INSERT INTO analysis_pool VALUES('ATLAS_13_HMDY','13TeV','Dileptons above the Z pole');
INSERT INTO analysis_pool VALUES('CMS_13_HMDY','13TeV','Dileptons above the Z pole');

-- three leptons
INSERT INTO analysis_pool VALUES('ATLAS_8_3L','8TeV','trileptons');         
INSERT INTO analysis_pool VALUES('CMS_8_3L','8TeV','trileptons');         
INSERT INTO analysis_pool VALUES('ATLAS_13_3L','13TeV','trileptons');         

-- four leptons
INSERT INTO analysis_pool VALUES('ATLAS_7_4L','7TeV','four leptons');         
INSERT INTO analysis_pool VALUES('ATLAS_8_4L','8TeV','four leptons');
INSERT INTO analysis_pool VALUES('ATLAS_13_4L','13TeV','four leptons');

INSERT INTO analysis_pool VALUES('ATLAS_7_LLMET','7TeV','dilepton+MET');         

-- two leptons plus gamma
INSERT INTO analysis_pool VALUES('ATLAS_7_EE_GAMMA','7TeV','e+e- plus photon(s)');
INSERT INTO analysis_pool VALUES('ATLAS_7_MM_GAMMA','7TeV','mu+mu- plus photon(s)');
INSERT INTO analysis_pool VALUES('ATLAS_8_EE_GAMMA','8TeV','e+e- plus photon(s)');
INSERT INTO analysis_pool VALUES('ATLAS_8_MM_GAMMA','8TeV','mu+mu- plus photon(s)');
INSERT INTO analysis_pool VALUES('ATLAS_13_LL_GAMMA','13TeV','dilepton plus photon');

-- lepton, MET (+optional jets)
INSERT INTO analysis_pool VALUES('ATLAS_7_LMETJET','7TeV','lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)'); 
INSERT INTO analysis_pool VALUES('ATLAS_7_EMETJET','7TeV','electron, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)'); 
INSERT INTO analysis_pool VALUES('ATLAS_7_MMETJET','7TeV','muon, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)'); 
INSERT INTO analysis_pool VALUES('CMS_7_EMETJET','7TeV','electron, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('CMS_7_MMETJET','7TeV','muon, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('ATLAS_8_LMETJET','8TeV','lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('ATLAS_8_MMETJET','8TeV','muon, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)'); 
INSERT INTO analysis_pool VALUES('ATLAS_8_EMETJET','8TeV','electron, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)'); 
INSERT INTO analysis_pool VALUES('CMS_8_MMETJET','8TeV','muon, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('CMS_8_LMETJET','8TeV','lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('ATLAS_13_LMETJET','13TeV','lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('CMS_13_MMETJET','13TeV','muon, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');
INSERT INTO analysis_pool VALUES('CMS_13_LMETJET','13TeV','lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)');

-- lepton, met, gamma
INSERT INTO analysis_pool VALUES('ATLAS_7_MMET_GAMMA','7TeV','muon, missing transverse momentum, plus photon'); 
INSERT INTO analysis_pool VALUES('ATLAS_7_EMET_GAMMA','7TeV','electron, missing transverse momentum, plus photon');

-- dilepton (non-resonant), met
INSERT INTO analysis_pool VALUES('ATLAS_7_L1L2MET','7TeV','WW analyses in dilepton plus missing transverse momentum channel');         
INSERT INTO analysis_pool VALUES('ATLAS_8_L1L2MET','8TeV','WW analyses in dilepton plus missing transverse momentum channel');
INSERT INTO analysis_pool VALUES('CMS_8_L1L2MET','8TeV','WW analyses in dilepton plus missing transverse momentum channel');
INSERT INTO analysis_pool VALUES('ATLAS_13_L1L2MET','13TeV','WW analyses in dilepton plus missing transverse momentum channel');


-- gamma plus MET
INSERT INTO analysis_pool VALUES('ATLAS_8_GAMMA_MET','8TeV','photon plus missing transverse momentum');

-- hadrons plus MET
INSERT INTO analysis_pool VALUES('ATLAS_13_METJET','13TeV','missing transverse momentum plus jets');



CREATE TABLE analysis (
    id      TEXT NOT NULL UNIQUE,
    lumi    REAL NOT NULL,
    pool    TEXT,
    PRIMARY KEY(id),
    FOREIGN KEY(pool) REFERENCES analysis_pool(pool)
);
-- Note, the LUMI value given here should be in units which match the normalisation
-- of the cross section plots. Most in pb, some in fb.

-- 13 TeV WW production cross-section
INSERT INTO analysis VALUES('ATLAS_2019_I1734263',36.1,'ATLAS_13_L1L2MET');    ----------------------------------

-- Superseded/deprecated analyses
INSERT INTO analysis VALUES('ATLAS_2012_I1083318',36.0,NULL);
INSERT INTO analysis VALUES('ATLAS_2011_I945498',36.0,NULL);
INSERT INTO analysis VALUES('ATLAS_2011_I921594',35.0,NULL);
INSERT INTO analysis VALUES('ATLAS_2011_S9128077',2.4,NULL);

-- 7 TeV fully hadronic
INSERT INTO analysis VALUES('ATLAS_2014_I1325553',4500.0,'ATLAS_7_JETS');
INSERT INTO analysis VALUES('ATLAS_2014_I1268975',4500.0,'ATLAS_7_JETS');
INSERT INTO analysis VALUES('ATLAS_2014_I1326641',4510.0,'ATLAS_7_JETS');
INSERT INTO analysis VALUES('ATLAS_2014_I1307243',4500.0,'ATLAS_7_JETS');
INSERT INTO analysis VALUES('CMS_2014_I1298810',5000.0,'CMS_7_JETS');
INSERT INTO analysis VALUES('CMS_2013_I1273574',37.0,'CMS_7_JETS');
INSERT INTO analysis VALUES('CMS_2013_I1208923',5000.0,'CMS_7_JETS');


-- 7 TeV photons
INSERT INTO analysis VALUES('ATLAS_2012_I1093738',37.0,'ATLAS_7_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2013_I1244522',37.0,'ATLAS_7_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2013_I1263495',4600.0,'ATLAS_7_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2012_I1199269',4900.0,'ATLAS_7_GAMMA');
INSERT INTO analysis VALUES('CMS_2014_I1266056',4500.0,'CMS_7_GAMMA');

-- 7 TeV high mass Drell Yan
INSERT INTO analysis VALUES('ATLAS_2013_I1234228',4900.0,'ATLAS_7_HMDY');


-- 7 TeV Z+jets
INSERT INTO analysis VALUES('ATLAS_2013_I1230812:LMODE=EL',4600.0,'ATLAS_7_EEJET');
INSERT INTO analysis VALUES('ATLAS_2013_I1230812:LMODE=MU',4600.0,'ATLAS_7_MMJET');
INSERT INTO analysis VALUES('ATLAS_2014_I1306294:LMODE=EL',4600.0,'ATLAS_7_EEJET');
INSERT INTO analysis VALUES('ATLAS_2014_I1306294:LMODE=MU',4600.0,'ATLAS_7_MMJET');
INSERT INTO analysis VALUES('CMS_2015_I1310737',4900.0,'CMS_7_LLJET');
INSERT INTO analysis VALUES('LHCB_2014_I1262703',1000.0,'LHCB_7_MMJET');
-- 7 TeV inclusive Z
INSERT INTO analysis VALUES('ATLAS_2016_I1502620:LMODE=ZEL',4600.0,'ATLAS_7_EEJET');
INSERT INTO analysis VALUES('ATLAS_2016_I1502620:LMODE=ZMU',4600.0,'ATLAS_7_MMJET');
INSERT INTO analysis VALUES('LHCB_2012_I1208102',940.0,'LHCB_7_EEJET');

-- 7 TeV ttbar --- TODO this can be split into E and MU channels
INSERT INTO analysis VALUES('ATLAS_2015_I1345452',4600.0,'ATLAS_7_LMETJET');

-- 7 TeV Low mass DY
INSERT INTO analysis VALUES('ATLAS_2014_I1288706',1600.0,'ATLAS_7_LMDY');

-- 7 TeV single jet masses: The rivet routines actually use only electrons, even though
-- the measurement used muons too. Not very confident about the normalisation.
INSERT INTO analysis VALUES('CMS_2013_I1224539:JMODE=W',5.0,'CMS_7_EMETJET');
INSERT INTO analysis VALUES('CMS_2013_I1224539:JMODE=Z',5.0,'CMS_7_EEJET');

-- 7 TeV W+jets. 
INSERT INTO analysis VALUES('ATLAS_2016_I1502620:LMODE=WEL',4600.0,'ATLAS_7_EMETJET');
INSERT INTO analysis VALUES('ATLAS_2016_I1502620:LMODE=WMU',4600.0,'ATLAS_7_MMETJET');
INSERT INTO analysis VALUES('ATLAS_2014_I1319490:LMODE=MU',4600.0,'ATLAS_7_MMETJET');
INSERT INTO analysis VALUES('ATLAS_2014_I1319490:LMODE=EL',4600.0,'ATLAS_7_EMETJET');
INSERT INTO analysis VALUES('CMS_2014_I1303894',5000.0,'CMS_7_MMETJET');

-- 7 TeV W+charm
INSERT INTO analysis VALUES('ATLAS_2014_I1282447',4600.0,'ATLAS_7_LMETJET');

-- 7 TeV W+b
INSERT INTO analysis VALUES('ATLAS_2013_I1219109:LMODE=EL',4.6,'ATLAS_7_EMETJET'); --W plus b jets
INSERT INTO analysis VALUES('ATLAS_2013_I1219109:LMODE=MU',4.6,'ATLAS_7_MMETJET'); --W plus b jets

-- 7 TeV Z+bb
INSERT INTO analysis VALUES('CMS_2013_I1256943',5000.0,'CMS_7_LLJET');

-- 7 TeV WW. Plots in fb.
INSERT INTO analysis VALUES('ATLAS_2013_I1190187',4.6,'ATLAS_7_L1L2MET'); -- jet veto is correctly applied in fiducial cuts.

-- 7 TeV dibosons, plots in fb 
INSERT INTO analysis VALUES('ATLAS_2013_I1217863:LMODE=ZEL',4.6,'ATLAS_7_EE_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2013_I1217863:LMODE=ZMU',4.6,'ATLAS_7_MM_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2013_I1217863:LMODE=WEL',4.6,'ATLAS_7_EMET_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2013_I1217863:LMODE=WMU',4.6,'ATLAS_7_MMET_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2012_I1203852:LMODE=LL',4.6,'ATLAS_7_4L');
INSERT INTO analysis VALUES('ATLAS_2012_I1203852:LMODE=LNU',4.6,'ATLAS_7_LLMET');

-- 8 TeV fully hadronic
-- plots in fb
INSERT INTO analysis VALUES('ATLAS_2015_I1394679',20.3,'ATLAS_8_JETS');
INSERT INTO analysis VALUES('ATLAS_2017_I1598613:BMODE=3MU',11.4,'ATLAS_8_JETS');
-- for the b hadron mode
-- plots in pb
INSERT INTO analysis VALUES('ATLAS_2017_I1604271',20200.0,'ATLAS_8_JETS');
INSERT INTO analysis VALUES('CMS_2016_I1487277',19700,'CMS_8_JETS');
INSERT INTO analysis VALUES('CMS_2017_I1598460',19700.,'CMS_8_JETS'); -- triple differential dijets

-- normalised, no total xsec yet INSERT INTO analysis VALUES('CMS_2016_I1421646',19700,'CMS_8_JETS');

-- 8 TeV photons
-- in pb
INSERT INTO analysis VALUES('ATLAS_2016_I1457605',20200.0,'ATLAS_8_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2017_I1632756',20200.0,'ATLAS_8_GAMMA'); -- +hf
-- in fb
INSERT INTO analysis VALUES('ATLAS_2017_I1591327',20.3,'ATLAS_8_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2014_I1306615',20.3,'ATLAS_8_GAMMA'); -- higgs to photons
INSERT INTO analysis VALUES('ATLAS_2017_I1644367',20.2,'ATLAS_8_GAMMA'); -- 3gamma (fb)


-- 8 TeV High mass DY
-- in pb
INSERT INTO analysis VALUES('ATLAS_2016_I1467454:LMODE=EL',20300.0,'ATLAS_8_HMDY_EL');
INSERT INTO analysis VALUES('ATLAS_2016_I1467454:LMODE=MU',20300.0,'ATLAS_8_HMDY_MU');

-- 8 TeV leptons+MET

-- normalised
INSERT INTO analysis VALUES('ATLAS_2014_I1279489',20300.0,'ATLAS_8_LLJET'); -- electroweak Z+jets
INSERT INTO analysis VALUES('ATLAS_2015_I1408516:LMODE=EL',20300.0,'ATLAS_8_EEJET'); -- Z production
INSERT INTO analysis VALUES('ATLAS_2015_I1408516:LMODE=MU',20300.0,'ATLAS_8_MMJET'); -- Z production
INSERT INTO analysis VALUES('ATLAS_2019_I1744201',19.9,'ATLAS_8_EEJET'); -- Z+jets
-- plots in pb
INSERT INTO analysis VALUES('ATLAS_2017_I1589844:LMODE=EL',20200.0,'ATLAS_8_EEJET');
INSERT INTO analysis VALUES('ATLAS_2017_I1589844:LMODE=MU',20200.0,'ATLAS_8_MMJET');
INSERT INTO analysis VALUES('CMS_2017_I1499471',11400,'CMS_8_LLJET');
INSERT INTO analysis VALUES('CMS_2016_I1491953',19600,'CMS_8_MMETJET');
INSERT INTO analysis VALUES('ATLAS_2015_I1404878',20200.0,'ATLAS_8_LMETJET');

-- ATLAS_2016_I1487726: collinear Wj at 8 TeV Cross sections, but the theory uncertainty is too large
-- to use it without the having the theory prediction

-- plots in fb 
INSERT INTO analysis VALUES('CMS_2016_I1454211',19.7,'CMS_8_LMETJET'); -- NB partonic phase space?
INSERT INTO analysis VALUES('ATLAS_2015_I1397637',20.3,'ATLAS_8_LMETJET'); -- ttbar
INSERT INTO analysis VALUES('ATLAS_2017_I1517194:LMODE=EL',20.2,'ATLAS_8_EMETJET'); -- electroweak W+jets 
INSERT INTO analysis VALUES('ATLAS_2017_I1517194:LMODE=MU',20.2,'ATLAS_8_MMETJET'); -- electroweak W+jets 
INSERT INTO analysis VALUES('ATLAS_2018_I1635273:LMODE=EL',20.2,'ATLAS_8_EMETJET'); -- W+jets. NB some plots in pb
INSERT INTO analysis VALUES('ATLAS_2018_I1635273:LMODE=MU',20.2,'ATLAS_8_MMETJET'); -- W+jets NB some plots in pb


-- plots in fb 
INSERT INTO analysis VALUES('CMS_2017_I1518399',19.7,'CMS_8_LMETJET');  -- TTBAR
INSERT INTO analysis VALUES('ATLAS_2016_I1426515',20.3,'ATLAS_8_L1L2MET'); -- nb jet veto is implemented in fiducial cuts.
INSERT INTO analysis VALUES('CMS_2017_I1467451',19.4,'CMS_8_L1L2MET'); -- nb b-jet veto is NOT implemented in fiducial cuts; and large data driven bg subtraction.

-- fb
INSERT INTO analysis VALUES('ATLAS_2015_I1394865',20.3,'ATLAS_8_4L');
INSERT INTO analysis VALUES('ATLAS_2014_I1310835',20.3,'ATLAS_8_4L'); -- HAVE THEORY
INSERT INTO analysis VALUES('ATLAS_2016_I1448301:LMODE=LL',20.3,'ATLAS_8_MM_GAMMA');
INSERT INTO analysis VALUES('ATLAS_2016_I1448301:LMODE=NU',20.3,'ATLAS_8_GAMMA_MET');
INSERT INTO analysis VALUES('ATLAS_2016_I1492320:LMODE=3L',20.3,'ATLAS_8_3L');
INSERT INTO analysis VALUES('ATLAS_2016_I1492320:LMODE=2L2J',20.3,'ATLAS_8_L1L2MET'); -- nb b-jet veto IS implemented in fiducial cuts
INSERT INTO analysis VALUES('ATLAS_2016_I1444991',20.3,'ATLAS_8_L1L2MET'); -- nb jet veto IS implemented in fiducial cuts; but large data-driven top subtraction

INSERT INTO analysis VALUES('CMS_2016_I1487288',1960.0,'CMS_8_3L');

-- 13 TeV DY. (fb)
INSERT INTO analysis VALUES('ATLAS_2019_I1725190',1.0,'ATLAS_13_HMDY'); -- lumi is set in routine, to give event counts
INSERT INTO analysis VALUES('CMS_2018_I1711625',2500,'CMS_13_HMDY');    -- plots in pb

-- 13 TeV fully hadronic
-- plots in pb
INSERT INTO analysis VALUES('ATLAS_2018_I1634970',3200.0,'ATLAS_13_JETS');
INSERT INTO analysis VALUES('ATLAS_2019_I1724098',33000.0,'ATLAS_13_JETS'); -- jet substructure.

INSERT INTO analysis VALUES('CMS_2016_I1459051',71.0,'CMS_13_JETS');
INSERT INTO analysis VALUES('CMS_2018_I1682495',2300.,'CMS_13_JETS'); -- jet mass

-- 13 TeV dileptons+jets
-- plots in pb
INSERT INTO analysis VALUES('ATLAS_2017_I1514251:LMODE=EL',3160.0,'ATLAS_13_EEJET'); -- Z+jets
INSERT INTO analysis VALUES('ATLAS_2017_I1514251:LMODE=MU',3160.0,'ATLAS_13_MMJET'); -- Z+jets
INSERT INTO analysis VALUES('CMS_2018_I1667854:LMODE=EL',2190.0,'CMS_13_EEJET'); -- Z+jets
INSERT INTO analysis VALUES('CMS_2018_I1667854:LMODE=MU',2190.0,'CMS_13_MMJET'); -- Z+jets
INSERT INTO analysis VALUES('CMS_2019_I1753680:LMODE=EL',2190.0,'CMS_13_EEJET'); -- Z production
INSERT INTO analysis VALUES('CMS_2019_I1753680:LMODE=MU',2190.0,'CMS_13_MMJET'); -- Z production

-- 13 TeV leptons+MET
-- plots in pb
INSERT INTO analysis VALUES('CMS_2017_I1610623',2200.0,'CMS_13_MMETJET'); -- W+jets
INSERT INTO analysis VALUES('CMS_2016_I1491950',2300.0,'CMS_13_LMETJET'); --ttbar
INSERT INTO analysis VALUES('CMS_2018_I1662081',35900.0,'CMS_13_LMETJET'); --ttbar
INSERT INTO analysis VALUES('CMS_2018_I1663958',35800,'CMS_13_LMETJET'); -- ttbar+jets (pb)
INSERT INTO analysis VALUES('CMS_2019_I1705068',35800.0,'CMS_13_MMETJET'); -- W+charm

INSERT INTO analysis VALUES('ATLAS_2017_I1614149',3200.0,'ATLAS_13_LMETJET'); --ttbar

-- plots in fb
INSERT INTO analysis VALUES('ATLAS_2017_I1625109',35.1,'ATLAS_13_4L');  -- ZZ
INSERT INTO analysis VALUES('ATLAS_2019_I1720442',35.1,'ATLAS_13_4L');  -- inclusive


-- Various BG regions from LQ search.
INSERT INTO analysis VALUES('ATLAS_2019_I1718132:LMODE=ELMU',35.1,'ATLAS_13_L1L2MET');
INSERT INTO analysis VALUES('ATLAS_2019_I1718132:LMODE=ELEL',35.1,'ATLAS_13_EEJET');
INSERT INTO analysis VALUES('ATLAS_2019_I1718132:LMODE=MUMU',35.1,'ATLAS_13_MMJET');

-- 13 TeV MET+JET
INSERT INTO analysis VALUES('ATLAS_2017_I1609448',3200.0,'ATLAS_13_METJET'); -- HAVE THEORY
INSERT INTO analysis VALUES('ATLAS_2016_I1458270',1.0,'ATLAS_13_METJET'); -- HAVE THEORY. 0l+MET+nJets search. Lumi is set to 3.2 in the routine
INSERT INTO analysis VALUES('ATLAS_2018_I1646686',36100.0,'ATLAS_13_TTHAD'); -- Hadronic top pairs (pb)
INSERT INTO analysis VALUES('ATLAS_2018_I1656578',3.2,'ATLAS_13_LMETJET'); --Top + jets 
INSERT INTO analysis VALUES('ATLAS_2018_I1705857',36.1,'ATLAS_13_LMETJET'); --Top + b jets (normalised)


-- 13 TeV photons
INSERT INTO analysis VALUES('ATLAS_2017_I1645627',3.2,'ATLAS_13_GAMMA');
-- 13 TeV photons+ll
INSERT INTO analysis VALUES('ATLAS_2019_I1764342',139.0,'ATLAS_13_LL_GAMMA'); -- Z+gamma


-- dodgy ATLAS 13TeV 3L
INSERT INTO analysis VALUES('ATLAS_2016_I1469071',3.2,'ATLAS_13_3L');



-- VARIOUS ANALYSIS WHICH HAVE BEEN CONSIDERED BUT NOT CURRENTLY USED
--
-- INSERT INTO analysis VALUES('CMS_2015_I1370682',  Particle-level, but don't have normalisation.
-- INSERT INTO analysis VALUES('ATLAS_2017_I1598613:BMODE=BB',11.4,'ATLAS_8_JETS'); cant use two modes in same pool. also, we don't have normalisation
-- INSERT INTO analysis VALUES('CMS_2013_I1224539_DIJET',5000.0,'CMS_7_JETS'); don't have normalisation
-- INSERT INTO analysis VALUES('CMS_2017_I1519995',2600,'CMS_13_JETS'); --TBD normalised cross sections
-- INSERT INTO analysis VALUES('CMS_2018_I1686000',1960.0,'CMS_8_3L'); -- huge bg subtraction, BDTs etc.
-- ATLAS_2017_I1626105  Dileptonic ttbar at 8 TeV
-- ATLAS_2017_I1604029 – ttbar + gamma at 8 TeV
-- LHCB_2018_I1665223 -- total cross section
-- CMS_2018_I1708620 -- soft QCD/energy density
-- ALICE_2012_I944757 -- charm in central rapidity region
-- ATLAS_2017_I1495243',3.2,'ATLAS_13_LMETJET'); --Top + jets, but all area normalised sadly, without providing the xsec. 
-- CMS_2018_I1690148 jet substr in top ALL NORMALISED, DONT USE
-- CMS_2016_I1421646 – Dijet azimuthal decorrelations in $pp$ collisions at $\sqrt{s} = 8$ TeV
-- commented out in python 'CMS_2013_I1256943',5200.0   CMS Z+b
-- Dileptonic emu tt early cross-section measurement at 13 TeV
-- INSERT INTO analysis VALUES('ATLAS_2016_I1468168' Inclusive only do not use.
-- off-resonance.  ATLAS_2018_I1677498 Would be nice but need normalisation
-- g->bb ATLAS_2018_I1711114  Would be nice but need normalisation



CREATE TABLE blacklist (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);

-- remove the area normalised plots
INSERT INTO blacklist VALUES('CMS_2019_I1753680:LMODE=EL','d3');
INSERT INTO blacklist VALUES('CMS_2019_I1753680:LMODE=MU','d3');

-- normalisation blacklisted New WW 2019                                   --------------------------------------------------------
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d22-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d25-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d28-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d31-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d34-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d37-x01-y01');

-- extrapolated to simplified phase space blacklisted New WW 2019

INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d41-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d42-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2019_I1734263','d43-x01-y01');




INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=EL','d02');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=EL','d04');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=EL','d06');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=EL','d08');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=MU','d02');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=MU','d04');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=MU','d06');
INSERT INTO blacklist VALUES('ATLAS_2013_I1230812:LMODE=MU','d08');
-- don't use the normalised plots
INSERT INTO blacklist VALUES('CMS_2013_I1273574','d10');
INSERT INTO blacklist VALUES('CMS_2013_I1273574','d11');
INSERT INTO blacklist VALUES('CMS_2013_I1273574','d12');

INSERT INTO blacklist VALUES('CMS_2014_I1298810','d13');
INSERT INTO blacklist VALUES('CMS_2014_I1298810','d14');
INSERT INTO blacklist VALUES('CMS_2014_I1298810','d15');
INSERT INTO blacklist VALUES('CMS_2014_I1298810','d16');
INSERT INTO blacklist VALUES('CMS_2014_I1298810','d17');
INSERT INTO blacklist VALUES('CMS_2014_I1298810','d18');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZEL','d17');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZEL','d18');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZEL','d20');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZMU','d17');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZMU','d18');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=ZMU','d20');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WMU','d15');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WMU','d16');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WMU','d19');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WEL','d15');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WEL','d16');
INSERT INTO blacklist VALUES('ATLAS_2013_I1217863:LMODE=WEL','d19');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d10-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d11-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d12-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d13-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d14-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1426515','d15-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2014_I1306615','d29');
INSERT INTO blacklist VALUES('ATLAS_2014_I1306615','d30');
-- ATLAS Z+jets ratios
INSERT INTO blacklist VALUES('ATLAS_2017_I1514251:LMODE=EL','d07-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2017_I1514251:LMODE=MU','d08-x01-y01');

INSERT INTO blacklist VALUES('CMS_2017_I1518399','d02');
INSERT INTO blacklist VALUES('CMS_2016_I1491953','d36');
INSERT INTO blacklist VALUES('CMS_2016_I1491953','d37');
INSERT INTO blacklist VALUES('CMS_2016_I1491953','d38');
INSERT INTO blacklist VALUES('CMS_2016_I1491953','d39');

INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d16');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d18');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d20');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d22');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d24');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d26');
INSERT INTO blacklist VALUES('ATLAS_2017_I1614149','d28');


-- have to veto all these at the moment because contur doesn't know
-- how to handle weighted differential xsecs presented as a 2D scatter.
INSERT INTO blacklist VALUES('ATLAS_2017_I1598613:BMODE=3MU','d01');

-- remove the normalised versions of the plots from CMS top.
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d42-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d44-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d46-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d48-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d50-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d52-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d54-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d56-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d58-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d59-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d60-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d61-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d63-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d64-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d65-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d66-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d68-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d69-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d70-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d71-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d73-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d74-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d75-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d76-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d78-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d79-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d80-x02-y01');
INSERT INTO blacklist VALUES('CMS_2016_I1491950','d81-x02-y01');
-- CMS Z+b ratios
INSERT INTO blacklist VALUES('CMS_2017_I1499471','d02-x01-y01');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','d04-x01-y01');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','d06-x01-y01');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','d08-x01-y01');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','d10-x01-y01');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','Z_pt');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','Dphi_Zj');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','first_jet');
INSERT INTO blacklist VALUES('CMS_2017_I1499471','HT');

-- ATLAS inclusive W/Z ratios
INSERT INTO blacklist VALUES('ATLAS_2016_I1502620:LMODE=WEL','d35-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2016_I1502620:LMODE=WEL','d35-x01-y02');
INSERT INTO blacklist VALUES('ATLAS_2016_I1502620:LMODE=WMU','d35-x01-y01');
INSERT INTO blacklist VALUES('ATLAS_2016_I1502620:LMODE=WMU','d35-x01-y02');

INSERT INTO blacklist VALUES('CMS_2018_I1662081','d01');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d02');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d03');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d04');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d05');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d06');
INSERT INTO blacklist VALUES('CMS_2018_I1662081','d07');

-- CMS jet mass normalised plots.
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d25');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d26');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d27');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d28');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d29');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d3');
INSERT INTO blacklist VALUES('CMS_2018_I1682495','d4');

-- BG subtracted tt bb
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d02');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d03');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d04');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d06');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d08');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d10');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d12');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d14');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d16');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d18');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d20');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d22');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d24');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d26');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d28');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d30');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d32');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d34');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d36');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d38');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d40');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d42');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d44');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d46');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d48');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d50');
INSERT INTO blacklist VALUES('ATLAS_2018_I1705857','d52');

INSERT INTO blacklist VALUES('CMS_2018_I1711625','d03'); -- DY extrapolated

-- ATLAS WZ "total cross section"
INSERT INTO blacklist VALUES('ATLAS_2016_I1469071','d06');

-- W+/- ratios
INSERT INTO blacklist VALUES('ATLAS_2018_I1635273:LMODE=EL','y03'); 
INSERT INTO blacklist VALUES('ATLAS_2018_I1635273:LMODE=MU','y03'); 


CREATE TABLE whitelist (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
-- W charm
INSERT INTO whitelist VALUES('ATLAS_2014_I1282447','d03-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1282447','d04-x01-y03');
--
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d01-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d01-x02-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d02-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d03-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d03-x02-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d04-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d04-x02-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d05-x03-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d05-x04-y01');
INSERT INTO whitelist VALUES('ATLAS_2014_I1279489','d05-x05-y01');
--
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d13');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d14');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d15');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d16');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d17');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d18');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d19');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d20');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d21');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d22');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d23');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d24');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d25');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d26');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d27');
INSERT INTO whitelist VALUES('ATLAS_2014_I1307243','d28');
--
INSERT INTO whitelist VALUES('CMS_2016_I1454211','d02');
INSERT INTO whitelist VALUES('CMS_2016_I1454211','d04');
INSERT INTO whitelist VALUES('CMS_2016_I1454211','d06');
INSERT INTO whitelist VALUES('CMS_2016_I1454211','d08');
--
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d01-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d02-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d03-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d04-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d05-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d06-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d07-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d08-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d09-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d10-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d11-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d12-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d13-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2018_I1646686','d14-x01-y01');
--
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d01-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d03-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d05-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d07-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d09-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d11-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d13-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d15-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d17-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d170-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d171-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d172-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d18-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d19-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d20-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d22-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d23-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d24-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d25-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d27-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d28-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d29-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d30-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d32-');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d33');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d34');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d35');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d37');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d38');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d39');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d40');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d42');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d43');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d44');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d45');

INSERT INTO whitelist VALUES('CMS_2018_I1663958','d47');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d48');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d49');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d50');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d51');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d52');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d53');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d54');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d56');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d57');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d58');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d59');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d60');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d61');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d62');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d63');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d65');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d66');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d67');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d68');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d69');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d70');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d71');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d72');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d74');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d75');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d76');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d77');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d78');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d79');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d80');
INSERT INTO whitelist VALUES('CMS_2018_I1663958','d81');
-- ttbar+jets
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d96');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d98');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d100');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d102');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d104');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d106');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d108');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d110');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d112');
INSERT INTO whitelist VALUES('ATLAS_2018_I1656578','d114');

-- EW W+jet
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d10');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d64');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d65');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d66');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d67');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d69');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d7');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d80');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d81');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d82');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d83');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d85');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d86');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d87');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d88');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d89');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d90');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d91');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d93');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d94');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d95');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d96');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d97');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d98');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d110');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d111');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d112');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d113');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d114');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d115');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d135');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d136');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d137');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d138');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d139');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d140');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d141');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d142');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d143');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d144');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d145');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d146');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d147');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d148');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=EL','d149');

INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d10');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d64');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d65');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d66');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d67');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d69');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d7');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d80');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d81');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d82');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d83');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d85');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d86');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d87');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d88');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d89');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d90');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d91');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d93');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d94');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d95');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d96');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d97');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d98');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d110');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d111');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d112');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d113');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d114');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d115');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d135');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d136');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d137');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d138');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d139');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d140');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d141');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d142');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d143');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d144');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d145');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d146');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d147');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d148');
INSERT INTO whitelist VALUES('ATLAS_2017_I1517194:LMODE=MU','d149');
-- H to WW
INSERT INTO whitelist VALUES('ATLAS_2016_I1444991','d02-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2016_I1444991','d03-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2016_I1444991','d04-x01-y01');
INSERT INTO whitelist VALUES('ATLAS_2016_I1444991','d05-x01-y01');
-- ttbar
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d01');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d03');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d05');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d07');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d09');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d11');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d13');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d15');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d17');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d19');
INSERT INTO whitelist VALUES('ATLAS_2015_I1404878','d21');
-- cms z+jets
INSERT INTO whitelist VALUES('CMS_2014_I1303894','d');
-- cms z+b
INSERT INTO whitelist VALUES('CMS_2013_I1256943','d');
-- ALTAS 0-lepton search
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d04');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d05');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d06');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d07');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d08');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d09');
INSERT INTO whitelist VALUES('ATLAS_2016_I1458270','d10');



CREATE TABLE subpool (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    subid   TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
-- 7 TeV
--INSERT INTO subpool VALUES('ATLAS_2012_I1203852','(d01-x01-y01|d01-x01-y03)',0); -- inc 4l vs 2l + met
--INSERT INTO subpool VALUES('ATLAS_2012_I1203852','(d03|d04)',1); -- pt 4l vs 2l + met
--INSERT INTO subpool VALUES('ATLAS_2012_I1203852','(d05|d06)',2); -- dphi 4l vs 2l + met
--INSERT INTO subpool VALUES('ATLAS_2012_I1203852','(d07|d08)',3); -- mT 4l vs 2l + met

INSERT INTO subpool VALUES('ATLAS_2014_I1325553','d01-x01-y0[1-6]',0); -- r=0.4 inclusive jet y bins
INSERT INTO subpool VALUES('ATLAS_2014_I1325553','d02-x01-y0[1-6]',1); -- r=0.6 inclusive jet y bins 
INSERT INTO subpool VALUES('ATLAS_2014_I1268975','d01-x01-y0[1-6]',0); -- r=0.4 dijet y* bins
INSERT INTO subpool VALUES('ATLAS_2014_I1268975','d02-x01-y0[1-6]',1); -- r=0.6 dijet y* bins
INSERT INTO subpool VALUES('ATLAS_2014_I1326641','d0[1-5]-x01-y01',0);       -- r=0.4 3 jet y* bins
INSERT INTO subpool VALUES('ATLAS_2014_I1326641','(d10|d0[6-9])-x01-y01',1); -- r=0.6 3 jet y* bins
INSERT INTO subpool VALUES('CMS_2014_I1298810','d0[1-6]-x01-y01',0);           -- r=0.5 y bins
INSERT INTO subpool VALUES('CMS_2014_I1298810','(d1[0-2]|d0[7-9])-x01-y01',1); -- r=0.7 y bins
INSERT INTO subpool VALUES('ATLAS_2014_I1307243','(d20|d1[3-9])-x01-y01',0);   -- deta jets Dy bins, inclusive
INSERT INTO subpool VALUES('ATLAS_2014_I1307243','d2[1-8]-x01-y01',1);         -- deta jets Dy bins, gap events
INSERT INTO subpool VALUES('ATLAS_2013_I1263495','d0[1-2]-x01-y01',0);         -- isolated photon eta bins
-- 8 TeV
INSERT INTO subpool VALUES('ATLAS_2016_I1457605','.',0); -- inclusive photon eta-gamma bins
INSERT INTO subpool VALUES('ATLAS_2017_I1604271','d0[1-6]-x01-y01',0);   -- inclusive jet pt, y bins, r=0.6
INSERT INTO subpool VALUES('ATLAS_2017_I1604271','(d1[0-2]|d0[7-9])',1); -- inclusive jet pt, y bins, r=0.4

INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=EL','d0[2-4]-x01-y01',0); -- low mass phi* distributions in eta bins
INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=EL','(d0[5-9]-x01-y01|d10-x01-y01)',1); -- medium mass phi* distributions in eta bins
INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=EL','d1[1-3]-x01-y01',2); -- medium mass phi* distributions in eta bins
INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=MU','d0[2-4]-x01-y04',3); -- low mass phi* distributions in eta bins
INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=MU','(d0[5-9]-x01-y04|d10-x01-y04)',4); -- low mass phi* distributions in eta bins
INSERT INTO subpool VALUES('ATLAS_2015_I1408516:LMODE=MU','d1[1-3]-x01-y04',5); -- low mass phi* distributions in eta bins

INSERT INTO subpool VALUES('CMS_2016_I1454211','d02|d06',0); -- top pt (el and mu)
INSERT INTO subpool VALUES('CMS_2016_I1454211','d04|d08',0); -- top y (el and mu)


-- 13 TeV
INSERT INTO subpool VALUES('ATLAS_2018_I1634970','d0[1-6]-x01-y01',0);   -- inclusive jet pt, y bins
INSERT INTO subpool VALUES('ATLAS_2018_I1634970','(d1[0-2]|d0[7-9])',1); -- dijet mass, y* bins

INSERT INTO subpool VALUES('CMS_2016_I1459051','d0[1-7]-x01-y01','07pTvy');   -- inclusive jet pt, y bins R=0.7
INSERT INTO subpool VALUES('CMS_2016_I1459051','(d1[0-4]|d0[8-9])','04pTvy'); -- inclusive jet pt, y bins R=0.4

INSERT INTO subpool VALUES('ATLAS_2019_I1725190','d0[1-2]-x01-y01','FlavourSplit'); -- HMDY e and mu distributions

INSERT INTO subpool VALUES('CMS_2018_I1711625','d0[5-6]-x01-y01','FlavourSplit'); -- HMDY e and mu distributions



INSERT INTO subpool VALUES('ATLAS_2019_I1720442','d01-x01-y01','M4Linclusive'); -- M4L inclusive
INSERT INTO subpool VALUES('ATLAS_2019_I1720442','d0[2-5]-x01-y01','M4LvpT'); -- M4L vs pT 
INSERT INTO subpool VALUES('ATLAS_2019_I1720442','d0[6-9]-x01-y01','M4Lvy'); -- M4L vs y
INSERT INTO subpool VALUES('ATLAS_2019_I1720442','d1[2-4]-x01-y01','M4LvFlavour'); -- M4L vs flavour



CREATE TABLE normalization (
--  analysis id, plot pattern, norm factor for ref data, number of x units an n_event distribution is differential in (nx, 0 if not applicable).
--  The normalisation is applied on the assumption the rivet plots have been area normalised to one, ie are
--  presented as 1/sigma(fid) * dsigma(fid)/dX where X is some kinematic variable. 
--  Therefore the norm factor number needs to be the integrated cross section for the fiducial region considered, 
--  and should be in pb since that is what rivet reports.
--  The nx applies for n_event distributions in searches which typically have no unecertainty in y. If the plot
--  is in e.g. n_events/10 GeV, to get the number of events, we multiply by the bin width and divide by nx; from this
--  the poisson (root(n)) uncertainty can be calculated. nx < 0 means the bin width is constant and the plot is just differential in that.

    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    norm    REAL NOT NULL,
    nxdiff INT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
-- CMS 3L is normalised in the rivet routine to 24.09 pb which is allegedly the WZ cross section
-- in the region.
-- Note that way the signal is dealt with in contur means this is turned into a cross section
-- normalisation just as though the scaling were to unity; the 24.09 makes rather than 1.0 makes
-- no difference. However, the data are presented as WZ cross sections, that is is (a) they are 
-- not nu lll cross sections but (b) and they not area normalised. What this means is we have to
-- turn them into nu lll cross sections to compare to the MC. This means multiplying by the
-- BR WZ --> lll nu where lll are eletrons or muons.
-- This is still a fudge, since we don't know the correction for the fiducial cuts, but that
-- should at least lead to an underestimate of any sensitivity.
INSERT INTO normalization VALUES('CMS_2016_I1487288','d',0.0363*2.0*0.1080*2.0,0); -- 3l

-- 7 TeV 4L (cross section and BF to leptons)
INSERT INTO normalization VALUES('ATLAS_2012_I1203852:LMODE=LL','(d03|d05|d07)',0.0254,0); -- 4l
INSERT INTO normalization VALUES('ATLAS_2012_I1203852:LMODE=LNU','(d04|d06|d08)',0.0127,0); -- 2l met

-- Normalisation from ATLAS aux material
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d0[1-6]' ,48200000.0/33000.0,0); -- dijet selection
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d0[7-9]',14400.0/33000.0,0); -- top
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d1[0-4]',14400.0/33000.0,0); -- top
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d1[5-9]',13900.0/33000.0,0); -- W selection
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d2[0-2]',13900.0/33000.0,0); -- W selection
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d2[3-8]',48200000.0/33000.0,0); -- dijet selection
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d29',14400.0/33000.0,0); -- top
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d3[0-6]',14400.0/33000.0,0); -- top
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d3[7-9]',13900.0/33000.0,0); -- W selection
INSERT INTO normalization VALUES('ATLAS_2019_I1724098','d4[0-4]',13900.0/33000.0,0); -- W selection



-- this is BR to a single charged lepton, needed when the xsec is report as a W 
-- and the generator reports the final state.
-- INSERT INTO normalization VALUES('ATLAS_2014_I1319490_MU','d',0.108059,0);
-- INSERT INTO normalization VALUES('ATLAS_2014_I1319490_EL','d',0.108059,0);
-- INSERT INTO normalization VALUES('CMS_2014_I1303894','d',0.108059,0);

-- these are the integrated cross section of the plot, in pb
INSERT INTO normalization VALUES('ATLAS_2014_I1279489','d01',5.88,0);
INSERT INTO normalization VALUES('ATLAS_2014_I1279489','d02',1.82,0);
INSERT INTO normalization VALUES('ATLAS_2014_I1279489','d03',1.10,0);
INSERT INTO normalization VALUES('ATLAS_2014_I1279489','d04',0.447,0);
INSERT INTO normalization VALUES('ATLAS_2014_I1279489','d05',0.066,0);
-- these are the integrated cross section of the plot, in pb
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d23',1.45,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d24',1.03,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d25',0.97,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d02',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d03',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d04',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d14',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d26',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d05',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d06',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d07',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d08',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d09',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d10',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d15',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d17',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d18',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d19',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d20',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d21',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d22',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d27',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d11',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d12',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d13',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d16',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=EL','d28',5.59,0);
-- these are the integrated cross section of the plot, in pb
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d23',1.45,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d24',1.03,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d25',0.97,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d02',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d03',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d04',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d14',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d26',14.96,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d05',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d06',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d07',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d08',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d09',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d10',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d15',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d17',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d18',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d19',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d20',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d21',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d22',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d27',537.10,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d11',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d12',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d13',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d16',5.59,0);
INSERT INTO normalization VALUES('ATLAS_2015_I1408516:LMODE=MU','d28',5.59,0);

-- CMS single jet mass stuff here
-- WJETS. Cross section in pb
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d52',1.06,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d56',1.06,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d60',1.06,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d64',1.06,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d68',1.06,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d53',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d57',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d61',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d65',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d69',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d72',2.3,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d54',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d58',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d62',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d66',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d70',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d73',0.962,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d55',0.43,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d59',0.43,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d63',0.43,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d67',0.43,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d71',0.43,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=W','d74',0.43,0);
-- ZJETS. Cross section in pb
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d29',0.852,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d33',0.852,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d37',0.852,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d41',0.852,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d45',0.852,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d30',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d34',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d38',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d42',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d46',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d49',1.22,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d31',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d35',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d39',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d43',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d47',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d50',0.377,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d32',0.141,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d36',0.141,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d40',0.141,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d44',0.141,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d48',0.141,0);
INSERT INTO normalization VALUES('CMS_2013_I1224539:JMODE=Z','d51',0.141,0);
-- 7 TeV WW cross section in fb
INSERT INTO normalization VALUES('ATLAS_2013_I1190187','d04-x01-y01',392.6,0);
-- 8 TeV b hadron cross section in fb
INSERT INTO normalization VALUES('ATLAS_2017_I1598613:BMODE=3MU','d',17700000,0);
-- 7 TeV LHCb stuff
INSERT INTO normalization VALUES('LHCB_2012_I1208102','d',76,0);
-- 20 GeV threshold
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d04-x01-y01',6.3,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d05-x01-y01',6.3,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d06-x01-y01',6.3,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d07-x01-y01',6.3,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d08-x01-y01',6.3,0);
-- 10 GeV threshold
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d03',16.0,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d04-x01-y02',16.0,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d05-x01-y02',16.0,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d06-x01-y02',16.0,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d07-x01-y02',16.0,0);
INSERT INTO normalization VALUES('LHCB_2014_I1262703','d08-x01-y02',16.0,0);

-- ttbb in fb
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d05-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d07-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d09-x01-y01',2450,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d11-x01-y01',2450,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d13-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d15-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d17-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d19-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d21-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d23-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d25-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d27-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d29-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d31-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d33-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d35-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d37-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d39-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d41-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d43-x01-y01',181,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d45-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d47-x01-y01',359,0);
INSERT INTO normalization VALUES('ATLAS_2018_I1705857','d49-x01-y01',359,0);
-- HMDY 13 TeV search
INSERT INTO normalization VALUES('ATLAS_2019_I1725190','d01-x01-y01',0,10);
INSERT INTO normalization VALUES('ATLAS_2019_I1725190','d02-x01-y01',0,10);
-- 13TeV SUSY MET 0-lepton search
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d04-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d05-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d06-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d07-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d08-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d09-x01-y01',0,-1);
INSERT INTO normalization VALUES('ATLAS_2016_I1458270','d10-x01-y01',0,-1);



-- This is a table to store any histograms which can only be used
-- if running in SM theory mode
CREATE TABLE metratio (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO metratio VALUES('ATLAS_2017_I1609448','d');


-- This is a table to store any histograms which can only be used
-- if running in SM theory mode. This includes searches, which require a background model
CREATE TABLE needtheory (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO needtheory VALUES('ATLAS_2017_I1609448','d');
INSERT INTO needtheory VALUES('ATLAS_2016_I1458270','d');
INSERT INTO needtheory VALUES('ATLAS_2019_I1725190','d');


-- This is a table to store any histograms which are made using
-- background-subtracted Higgs gamma-gamma signal and which
-- should therefore not be used to exclude continuum diphotons 
CREATE TABLE higgsgg (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO higgsgg VALUES('ATLAS_2014_I1306615','d');

-- This is a table to flag search distributions (ie not unfolded measurements) so they
-- can be turned off if desired
CREATE TABLE searches (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO searches VALUES('ATLAS_2016_I1458270','d');
INSERT INTO searches VALUES('ATLAS_2019_I1725190','d');

-- This is a table to store any histograms which are made using
-- background-subtracted Higgs ww signal and which
-- should therefore not be used to exclude bsm top/w production
CREATE TABLE higgsww (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO higgsww VALUES('CMS_2017_I1467451','d');
INSERT INTO higgsww VALUES('ATLAS_2016_I1444991','d');

-- This is a table to store ATLAS WZ measurements which assume the SM
CREATE TABLE atlaswz (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO atlaswz VALUES('ATLAS_2016_I1469071','d');

-- This is a table to store measurements which have a secret b-jet vetp
CREATE TABLE bveto (
    id      TEXT NOT NULL,
    pattern TEXT NOT NULL,
    UNIQUE(id,pattern),
    FOREIGN KEY(id) REFERENCES analysis(id)
);
INSERT INTO bveto VALUES('CMS_2014_I1303894','d');
INSERT INTO bveto VALUES('CMS_2016_I1491953','d');
INSERT INTO bveto VALUES('CMS_2017_I1610623','d');
INSERT INTO bveto VALUES('CMS_2017_I1467451','d');
--

COMMIT;
