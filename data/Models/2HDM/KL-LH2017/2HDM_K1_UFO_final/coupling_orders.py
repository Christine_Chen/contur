# This file was automatically created by FeynRules 2.3.3
# Mathematica version: 10.0 for Linux x86 (64-bit) (December 4, 2014)
# Date: Sat 17 Jun 2017 13:56:34


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

YB = CouplingOrder(name = 'YB',
                   expansion_order = 99,
                   hierarchy = 1)

YT = CouplingOrder(name = 'YT',
                   expansion_order = 99,
                   hierarchy = 1)

