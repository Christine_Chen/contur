# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 11.0.1 for Linux x86 (64-bit) (September 21, 2016)
# Date: Tue 29 May 2018 17:21:51


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

YB = CouplingOrder(name = 'YB',
                   expansion_order = 99,
                   hierarchy = 1)

YT = CouplingOrder(name = 'YT',
                   expansion_order = 99,
                   hierarchy = 1)

