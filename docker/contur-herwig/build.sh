#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.2
HERWIG_VERSION=7.1.2

for CONTUR_BRANCH in master contur-{1.2.3,1.2.x}; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}

    MSG="Building contur-herwig with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, Herwig=$HERWIG_VERSION"
    tag="hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION"

    docker build . -f Dockerfile --build-arg CONTUR_BRANCH=$CONTUR_BRANCH -t $tag
    docker tag $tag hepstore/contur-herwig:$CONTUR_VERSION

    if [[ "$PUSH" = 1 ]]; then
        docker push $tag && sleep 30s
        docker push hepstore/contur-herwig:$CONTUR_VERSION && sleep 30s
    fi
done

# docker tag $tag hepstore/contur-herwig:latest
# if [[ "$PUSH" = 1 ]]; then
#     docker push hepstore/contur-herwig:latest
# fi
