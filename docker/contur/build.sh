#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.2

for CONTUR_BRANCH in master contur-{1.2.3,1.2.x}; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}

    MSG="Building contur with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION"
    tag="hepstore/contur:$CONTUR_VERSION"

    docker build . -f Dockerfile --build-arg CONTUR_BRANCH=$CONTUR_BRANCH -t $tag

    if [[ "$PUSH" = 1 ]]; then
        docker push $tag && sleep 30s
    fi
done

# docker tag $tag hepstore/contur:latest
# if [[ "$PUSH" = 1 ]]; then
#     docker push hepstore/contur:latest
# fi
