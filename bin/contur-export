#!/usr/bin/env python

from __future__ import print_function
import os
import sys
from contur.export import export
import argparse

input_file_help = 'Input path to load the Contur Map file from, relative to the current working directory or absolute'
output_file_help = 'Output path to save the CSV file to, relative to the current working directory or absolute'
dominant_pools_help = 'Add a column to the exported CSV that includes the dominant pool for the confidence level calculation of each point.'
description = 'contur-export converts a Contur Map file (a pickled Depot class instance) into a CSV that includes the values for each parameter, the CL, and optionally the dominant pool.'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-i', '--input', help=input_file_help, required=True)
    parser.add_argument('-o', '--output', help=output_file_help, required=True)
    parser.add_argument('-dp', '--dominant-pools',
                        help=dominant_pools_help, action="store_true")
    args = parser.parse_args()

    export(args.input, args.output, include_dominant_pools=args.dominant_pools)
