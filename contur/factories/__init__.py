"""
The factories package contains the collection of modules defining the analysis data flow

The factories module contains three submodules:

* :mod:`~contur.factories.depot` -- Contains the high level analysis steering classes

* :mod:`~contur.factories.yoda_factories` -- Contains classes to process ``YODA`` objects and control data flow between :mod:`~contur.factories.depot` and :mod:`~contur.factories.likelihood`

* :mod:`~contur.factories.likelihood` -- Contains classes to construct likelihood functions from numerical inputs
"""

from .depot import ParamYodaPoint, Depot
from .likelihood import Likelihood, CombinedLikelihood
from .yoda_factories import HistFactory, YodaFactory, init_ref
