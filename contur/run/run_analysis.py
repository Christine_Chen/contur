import rivet

import sys
import os
import pickle
import logging

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import contur


def get_argparser():
    parser = ArgumentParser(
        usage=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)

    io = parser.add_argument_group("Input/Output control options")
    dress = parser.add_argument_group("Dressing options to embelish outputs")
    select = parser.add_argument_group(
        "Options to exclude/include subsets of data")
    stats = parser.add_argument_group(
        'Options to Manipulate the constructed test statistic')

    parser.add_argument("-v", "--version", action="store_true", dest="printVersion",
                        default=False, help="print version number and exit.")
    parser.add_argument("-d", "--debug", action="store_true", dest="DEBUG", default=False,
                        help="Switch on Debug to all, written to contur.log")
    parser.add_argument("-q", "--quiet", action="store_true", dest="QUIET", default=False,
                        help="Suppress info messages")

    io.add_argument('yoda_files', nargs='*', metavar='yoda_files',
                    help='.yoda files to process.')
    io.add_argument("-o", "--outputdir", dest="OUTPUTDIR",
                    default="plots", help="Specify output directory for output.")
    io.add_argument("-a", "--analysisdir", dest="ANALYSISDIR",
                    default="ANALYSIS", help="Output directory for analysis "
                    "cards.")
    io.add_argument("-y", "--yodamerge",
                    action="store_true", dest="MERGEONLY", default=False,
                    help="only do the yodamerge step, then exit ")
    io.add_argument("-n", "--nostack",
                    action="store_true", dest="NOSTACK", default=False,
                    help="in single run mode, do not stack the histograms in dat "
                    "file output")
    io.add_argument("-g", "--grid", dest="GRID", default=None,
                    help="specify a folder containing a structured grid of points"
                          " to analyse. Usually 'myscan'.")
    io.add_argument('-T', '--tag', dest='TAG', default='runpoint_',
                    help='Identifier for yoda files when using yoda_merger.')
    io.add_argument("--new", action="store_true",
                    help="Force Contur to not use possibly available merges of yoda files but merge yoda files anew.")

    dress.add_argument("-p", "--param_file", dest="PARAM_FILE", default="params.dat",
                       help="Optionally specify a parameter file. Only used for documentation, and ignored for grid running.")
    dress.add_argument("-m", "--model", dest="MODEL",
                       help="Optionally give name for model used. Only used for documentation.")
    dress.add_argument("--bf", "--branchingfraction", dest="BF",
                       help="Optionally give a comma-separated list of branching fractions for which cross sections will be stored in the map file")
    dress.add_argument("--me", "--matrixelement", dest="ME",
                       help="Optionally give a comma-separated list of matrix elements for which cross sections will be stored in the map file")
    dress.add_argument("-w", "--width", dest="WIDTH",
                       help="Optionally give a comma-separated list of particle widths for which values will be stored in the map file")
    dress.add_argument("-S", "--slha", dest="SLHA", default="MASS",
                       help="read parameters from a comma-sperated list of blocks in an SLHA file")
    dress.add_argument("-M", "--mass", dest="MASS",
                       help="Optionally give a comma-separated list of particle masses for which values will be stored in the map file")
    dress.add_argument("--BW", "--binwidth", dest="BINWIDTH",
                       help="optional binning of SLHA paramters")
    dress.add_argument("--BO", "--binoffset", dest="BINOFFSET",
                       help="optional bin offset for SLHA parameters")

    select.add_argument("--xr", "--nometratio",
                        action="store_true", dest="EXCLUDEMETRAT", default=False,
                        help="Exclude plots where exclusion would be based on a ratio of met/dielptons"
                               "Use this when you have ehnance Z production in your model.")
    select.add_argument("--xhg", "--nohiggsgamma",
                        action="store_true", dest="EXCLUDEHGG", default=False,
                        help="Exclude plots where Higgs to photons signal is background-subtracted by fitting continuum."
                               "Do this when you have large non-Higgs diphoton production from your model.")
    select.add_argument("--whw", "--withhiggsww",
                        action="store_true", dest="USEHWW", default=False,
                        help="Include plots where Higgs to WW signal is background-subtracted using data."
                               "Only try this when you have large Higgs WW from your model and not much top or other source of WW.")
    select.add_argument("--wbv", "--withbvetos",
                        action="store_true", dest="USEBV", default=False,
                        help="Include plots where a b-jet veto was applied in the measurement but not in the fiducial definition."
                               "Only try this when you have large W+jets enhancements and no extra top or other source of W+b.")
    select.add_argument("--awz", "--atlas-wz",
                        action="store_true", dest="USEAWZ", default=False,
                        help="Include the ATLAS WZ analysis with dodgy SM assumptions."
                               "Might be useful for enhanced WZ cross sections but be careful.")
    select.add_argument("-s", "--use-searches",
                        action="store_true", dest="USESEARCHES", default=False,
                        help="Use reco-level search analyses in the sensitivity evaluation (beta).")
    select.add_argument("--wn", "--weight-name", dest="WEIGHTNAME", default="",
                        help="for weighted events/histos, select the name of the weight to use.")
    select.add_argument("--to", "--theoryonly", action="store_true", dest="THONLY", default=False,
                        help="Only use data where theory prediction is known")
    select.add_argument("--ana-match", action="append", dest="ANAPATTERNS", default=[],
                        help="only run on analyses whose name matches any of these regexes")
    select.add_argument("--ana-unmatch", action="append", dest="ANAUNPATTERNS", default=[],
                        help="exclude analyses whose name matches any of these regexes")
    select.add_argument("--split-pools", action="append", dest="POOLPATTERNS", default=[],
                        help="write out histograms from analyses in given pools separately")
    select.add_argument("--ana-split", action="append", dest="ANASPLIT", default=[],
                        help="write out histograms from given analyses separately")
    select.add_argument("-b", "--beams", action="append", dest="BEAMS", default=[],
                        help="in grid mode, only run on these beams")
    select.add_argument("-f", "--findPoint", action="append", dest="FINDPARAMS", default=[],
                        help="identify points consistent with these parameters and make histograms for them")

    stats.add_argument("-c", "--correlations", action="store_true", dest="CORR", default=False,
                       help="Turn on all treatment of correlations (beta)")
    stats.add_argument("-e", "--min_syst", dest="MIN_SYST", default=0.001,
                       help="Correlated systematic errors with a maximum fractional contribution below this will be ignored (saves time!)")
    stats.add_argument("--ep", "--error_precision", dest="ERR_PREC", default=0.01,
                       help="precision cut off in nuisance parameters when minimizing LL")
    stats.add_argument("--lp", "--ll_precision", dest="LL_PREC", default=0.01,
                       help="precision cut off in LL when minimizing it")
    stats.add_argument("--n-iter", dest="N_ITER", default=20,
                       help="minimize cuts off after n_iter*n_variables iterations")
    stats.add_argument("--min_num_sys", dest="MNS", default=2,
                       help="minimum number of systematic nuisance parameters for them to be treated as correlated")

    stats.add_argument("--th", "--theory", action="store_true", dest="THY", default=False,
                       help="Use theory background models (n.b. ratios will still be used w. theory)")
    stats.add_argument("--el", "--expected_limits", action="store_true", dest="EXPECTED", default=False,
                       help="Ignore the data, and calculate the expected limits where SM theory is available.")
    stats.add_argument("--xtc", "--notheorycorr", action="store_true", dest="XTHCORR", default=False,
                       help="Assume theory uncertainties are uncorrelated")

    return parser


def get_args(argv):
    """Parse command line arguments"""
    parser = get_argparser()
    args = parser.parse_args(argv)
    return args


def write_output(message, conturDepot, args):
    """Temporary function to mimic output of 1 single analysis"""
    contur.util.mkoutdir(args.ANALYSISDIR)
    sumfn = open(args.ANALYSISDIR + "/Summary.txt", 'w')

    if contur.config.gridMode:
        sumfn.write(message)
    else:

        # summary function will just read the first entry in the depot inbox
        if conturDepot.inbox[0].yoda_factory._full_likelihood.CLs is not None:
            result = "Combined exclusion for these plots is %.2f %% \n" % (
                    conturDepot.inbox[0].yoda_factory._full_likelihood.CLs * 100.0)
        else:
            result = "Could not evaluate exclusion for these data. Try turning off theory correlations?"

        sumfn.write(message + "\n" + result + "\n")
        sumfn.write("pools")
        for x in conturDepot.inbox[0].yoda_factory._sorted_likelihood_blocks:
            sumfn.write("\n" + x.pools)
            sumfn.write("\n" + str(x.CLs))
            sumfn.write("\n" + rivet.stripOptions(x.tags))

        contur.config.contur_log.info(result)

        sumfn.close()


def write_output_grid(gridPoints, args):
    contur.util.mkoutdir(args.ANALYSISDIR)
    path_out = os.path.join(args.ANALYSISDIR, args.GRID.strip(os.sep) + '.map')

    with open(path_out, 'w') as f:
        pickle.dump(gridPoints, f)
    contur.config.contur_log.info("Writing output map for " + args.GRID.rstrip(os.sep) + " to : " +
                      path_out)

def yoda_merger(scan_dir, tag="runpoint_"):
    """Move through directories merging yoda analysis files"""
    contur.config.contur_log.info("Merging yoda files (%s)\n"
                      "-------------------" % scan_dir)
    for root, dirs, files in sorted(os.walk(scan_dir)):
        file_list = []
        file_list_to_zip = []  # separate list to prevent zipping already zipped files again
        run_point_num = os.path.basename(root)
        out_file = os.path.join(root, tag + run_point_num + '.yoda')
        out_file_gz = out_file+'.gz'

        if not os.path.exists(out_file_gz):
            if os.path.exists(out_file):
                # runpoint file was there but uncompressed. Compress it.
                command = ' '.join(['gzip ', out_file])
                contur.config.contur_log.info(command)
                os.system(command)
            else:
                # runpoint file was not there. Make it.

                for name in files:
                    conditions = ((name.endswith('.yoda') or name.endswith('.yoda.gz')) and
                                  name != 'LHC.yoda' and
                                  'LHC' in name and
                                  tag in name)
                    if conditions:
                        yodafile = os.path.join(root, name)
                        file_list.append(yodafile)
                        if name.endswith('.yoda'):
                            file_list_to_zip.append(yodafile)

                if file_list:
                    file_string = ' '.join(file_list)
                    if len(file_list) > 1:
                        command = ' '.join(
                            ['yodamerge -o', out_file, file_string])
                    else:
                        command = ' '.join(['cp', file_string, out_file])
                    contur.config.contur_log.info(command)
                    os.system(command)

                    command = ' '.join(['gzip -f', out_file]+file_list_to_zip)
                    contur.config.contur_log.info(command)
                    os.system(command)

                else:
                    contur.config.contur_log.warning(
                        'NO YODA FILES FOUND IN DIRECTORY ' + run_point_num)
        else:
            contur.config.contur_log.warning(tag + run_point_num +
                                 '.yoda.gz already exists.')


# process the grid with the given options
def process_grid(args, pool=None, mergedDirs=[]):
    if pool is not None:
        contur.config.contur_log.info(
            "Processing grid for pool {}, analysis {}".format(pool, contur.config.onlyAnalyses))

    depots = {}
    # look for beams subdirectories in the chosen grid and analyse each of them
    known_beams = contur.data.getBeams(pool)
    for beam in known_beams:
        beam_dir = os.path.join(args.GRID, beam)

        if (beam in args.BEAMS or len(args.BEAMS) == 0) and os.path.exists(beam_dir):

            # merge/rename all the yoda files for each beam and parameter point
            if not beam in mergedDirs:

                contur.scan.grid_loop(scan_path=beam_dir, unmerge=args.new)
                yoda_merger(beam_dir, args.TAG)
                mergedDirs.append(beam)

            contur_depot = contur.factories.Depot(
                outputDir=args.OUTPUTDIR, noStack=args.NOSTACK)
            analyse_grid(os.path.abspath(beam_dir), contur_depot, args)
            # save some memory
            contur_depot.resort_points()
            depots[beam_dir] = contur_depot

        # RESET the refload flag for the next beam
        contur.config.refLoaded = False

    if len(depots) > 0:
        # merge maps for each beam
        contur.config.contur_log.info("Merging maps")
        target = None
        for depot in depots.values():
            if not target:
                target = depot
                continue
            else:
                target.merge(depot)

        if target:
            target.resort_points()
        target.write(args.ANALYSISDIR)

    elif len(args.BEAMS) == 0:

        # look for yoda files in the top level directory and analyse them.
        ctdp = contur.factories.Depot(outputDir=args.OUTPUTDIR, noStack=args.NOSTACK)
        analyse_grid(os.path.abspath(args.GRID), ctdp, args)

        ctdp.write(os.path.join(args.ANALYSISDIR))

    else:
        contur.config.contur_log.info("No compatible yoda files found.")

#import multiprocessing
#import pathos.pools as pp


def analyse_grid(scan_path, conturDepot, args):
    # grid_points = []
    yoda_counter = 0
    yodapaths = []
    parampaths = []

    for root, dirs, files in sorted(os.walk(scan_path)):

        valid_yoda_file = False
        for file_name in files:
            valid_yoda_file = ((file_name.endswith('.yoda')
                               or file_name.endswith('.yoda.gz'))
                               and
                               'LHC' not in file_name and
                               'run' in file_name)
            if valid_yoda_file: 
                yoda_counter += 1
                yoda_file_path = os.path.join(root, file_name)
                break

        if not valid_yoda_file:
            continue

        param_file_path = os.path.join(root, contur.config.paramfile)
        yodapaths.append(yoda_file_path)
        parampaths.append(param_file_path)
        contur.config.contur_log.debug(
            'Reading parameters from {}'.format(param_file_path))
        params = contur.util.read_param_point(param_file_path)
        contur.config.contur_log.info('Found valid yoda file ' +
                                  yoda_file_path.strip('./'))
        sample_str = 'Sampled at:'
        for param, val in params.items():
            sample_str += ('\n'+param + ': ' + str(val))

            if args.SLHA and param=="slha_file":
                block_list = args.SLHA.split(",")
                # read parameters from blocks in an SLHA file
                block_dict = contur.util.read_slha_file(root, val, block_list)
                for block in block_list:
                    params.update(block_dict[block])

        contur.config.contur_log.info(sample_str)

        # If requested, grab some values from the log file and add them as extra parameters.
        if args.ME:
            params.update(contur.util.read_out_file(
                    root, files, args.ME.split(",")))
        bf_list = None
        width_list = None
        mass_list = None
        if args.BF is not None:
            bf_list = args.BF.split(",")
        if args.WIDTH is not None:
            width_list = args.WIDTH.split(",")
        if args.MASS is not None:
            mass_list = args.MASS.split(",")

        if args.BF or args.WIDTH or args.MASS:
            params.update(contur.util.read_log_file(
                    root, files, bf_list, width_list, mass_list))


        # Perform analysis
        conturDepot.add_point(
            param_dict=params, yodafile=yoda_file_path)

    contur.config.contur_log.info("Found %i yoda files" % yoda_counter)
    conturDepot.build_axes()


def main(args):
    modeMessage = "Run Information \n"

    modeMessage += "Contur is running in {} \n".format(os.getcwd())
    if args.GRID:
        modeMessage += "on files in {} \n".format(args.GRID)
    else:
        modeMessage += "on analysis objects in {} \n".format(args.yoda_files)

    if args.EXCLUDEHGG:
        contur.config.excludeHgg = True
        modeMessage += "Excluding Higgs to photons measurements \n"

    if args.USESEARCHES:
        contur.config.excludeSearches = False
        modeMessage += "Using search analyses \n"

    if args.USEHWW:
        contur.config.excludeHWW = False
        modeMessage += "Including Higgs to WW measurements if available \n"
    else:
        contur.config.excludeHWW = True
        modeMessage += "Excluding Higgs to WW measurements \n"

    if args.USEBV:
        contur.config.excludeBV = False
        modeMessage += "Including secret b-veto measurements if available \n"
    else:
        contur.config.excludeBV = True
        modeMessage += "Excluding secret b-veto measurements \n"

    if args.USEAWZ:
        contur.config.excludeAWZ = False
        modeMessage += "Including ATLAS WZ SM measurement \n"
    else:
        contur.config.excludeAWZ = True
        modeMessage += "Excluding ATLAS WZ SM measurement \n"

    if args.EXCLUDEMETRAT:
        contur.config.excludeMETRatio = True
        modeMessage += "Excluding MET ratio measurements \n"

    if args.printVersion:
        contur.util.writeBanner()
        sys.exit(0)

    if args.GRID is None:
        contur.config.gridMode = False
    else:
        contur.config.gridMode = True

    if not args.yoda_files and not contur.config.gridMode:
        contur.config.contur_log.critical("Error: You need to specify some YODA files to be "
                              "analysed!\n")
        sys.exit(1)

    if args.WEIGHTNAME is not None:
        contur.config.weight = args.WEIGHTNAME

    contur.config.contur_log.setLevel(logging.INFO)
    contur.util.writeBanner()
    if args.QUIET:
        contur.config.contur_log.setLevel(logging.WARNING)
    if args.DEBUG:
        contur.config.contur_log.setLevel(logging.DEBUG)

    # Set the global args used in config.py
    if args.PARAM_FILE:
        contur.config.paramfile = args.PARAM_FILE

    if args.CORR:
        contur.config.buildCorr = True
        modeMessage += "Building all available data correlations, combining bins where possible \n"
        if not contur.config.min_syst == float(args.MIN_SYST):
            contur.config.min_syst = float(args.MIN_SYST)
            modeMessage += "Systematic materiality cutoff changed to {} \n".format(
                contur.config.min_syst)

        if not contur.config.ll_prec == float(args.LL_PREC):
            contur.config.ll_prec = float(args.LL_PREC)
            modeMessage += "LL precision criterion changed to  {} \n".format(
                contur.config.ll_prec)

        if not contur.config.n_iter == int(args.N_ITER):
            contur.config.n_iter = int(args.N_ITER)
            modeMessage += "max number of iterations changed to  {} \n".format(
                contur.config.n_iter)

        if not contur.config.err_prec == float(args.ERR_PREC):
            contur.config.err_prec = float(args.ERR_PREC)
            modeMessage += "Precision cut off in nuisance parameters when minimizing LL changed to {} \n".format(
                contur.config.err_prec)

        if not args.MNS == int(args.MNS):
            contur.config.min_num_sys = int(args.MNS)
            modeMessage += "Minimum number of systematic uncertainties contributions for correlations changed to {} \n".format(
                contur.config.min_num_sys)

    else:
        contur.config.buildCorr = False
        modeMessage += "No correlations being built, using single bins in tests \n"

    if args.THY or args.EXPECTED:
        contur.config.useTheory = True
        modeMessage += "Using theory calculations for background model where available. \n"
    else:
        contur.config.useTheory = False
        modeMessage += "Building default background model from data, ignoring (optional) theory predictions \n"

    if args.THONLY:
        contur.config.theoryOnly = True
        modeMessage += "Only use measurements where theory calculations for background model are available. \n"
    else:
        contur.config.theoryOnly = False

    if args.EXPECTED:
        contur.config.expectedLimit = True
        contur.config.theoryOnly = True
        modeMessage += "Only calculating expected limits -- ignoring the actual data. \n"

    if args.XTHCORR:
        contur.config.useTheoryCorr = False
        if not contur.config.useTheory:
            contur.config.contur_log.critical(
                "Option clash, requested uncorrelated theory uncertainty but not using theory")
            sys.exit(1)

        modeMessage += "Theory uncertainties assumed uncorrelated. \n"
    elif contur.config.useTheory:
        contur.config.useTheoryCorr = True
        modeMessage += "Theory uncertainty correlations used. \n"

    if args.ANAPATTERNS:
        contur.config.onlyAnalyses = args.ANAPATTERNS
        modeMessage += "Only using analysis objects whose path includes %s. \n" % args.ANAPATTERNS
    if args.ANASPLIT:
        contur.config.splitAnalysis = True
        modeMessage += "Splitting these analyses into seperate histograms %s. \n" % args.ANASPLIT
    if args.ANAUNPATTERNS:
        contur.config.vetoAnalyses = args.ANAUNPATTERNS
        modeMessage += "Excluding analyses names: %s. \n" % args.ANAUNPATTERNS
    if args.POOLPATTERNS:
        modeMessage += "Splitting analyses of pools %s. \n" % args.POOLPATTERNS

    if args.BINWIDTH:
        contur.config.binwidth = float(args.BINWIDTH)
    if args.BINOFFSET:
        contur.config.binoffset = float(args.BINOFFSET)

    contur.config.contur_log.info(modeMessage)

    ctdp = contur.factories.Depot(outputDir=args.OUTPUTDIR, noStack=args.NOSTACK)

    # rather than porting arguments though class instance initialisations, instead set these as variables in the global config.py
    # all these are imported on import contur so set them here and pick them up when needed later

    if args.MODEL:
        modeMessage += '\n Model: ' + args.MODEL
        contur.config.contur_log.info('\n Model: ' + args.MODEL)
    if contur.config.gridMode:
        # grid mode
        # --------------------------------------------------------------------------------------------------
        contur.config.contur_log.removeFilter(contur.config.dup_filter)
        mergedDirs = []
        if args.POOLPATTERNS:
            # In this case we are running on specified pools and breaking them down into separate analyses
            anaDir = args.ANALYSISDIR
            for pool in args.POOLPATTERNS:
                anas = contur.data.getAnalyses(pool)
                for a in anas:
                    if a in contur.config.vetoAnalyses:
                        continue
                    contur.config.onlyAnalyses = args.ANAPATTERNS + \
                        [a]  # add analysis to must-match anas
                    # setup a different directory for each ana
                    args.ANALYSISDIR = os.path.join(anaDir, pool, a)
                    process_grid(args, pool, mergedDirs)
            args.ANALYSISDIR = anaDir  # reset ANALYSIDIR to original value
        elif contur.config.splitAnalysis:
            # In this case we are running on specified analyses and breaking them down into histos/subpools.
            anaDir = args.ANALYSISDIR
            # One analysis at a time
            for ana in args.ANASPLIT:
                contur.config.contur_log.info(
                    'Running grid on {} and splitting it into pools'.format(ana))
                # setup a different directory for each ana
                args.ANALYSISDIR = os.path.join(anaDir, ana)
                contur.config.onlyAnalyses = args.ANAPATTERNS + [ana]
                # for subpool/hist etc
                process_grid(args, None, mergedDirs)

            args.ANALYSISDIR = anaDir  # reset ANALYSIDIR to original value

        else:
            # In this case we are running on everything
            process_grid(args)

        write_output(modeMessage, ctdp, args)

    else:
        # single mode
        # --------------------------------------------------------------------------------------------------

        # find the specified parameter point.
        yodaFiles = contur.util.findParamPoint(
            args.yoda_files, args.TAG, args.FINDPARAMS)

        for infile in yodaFiles:

            anaDir = args.ANALYSISDIR
            outDir = args.OUTPUTDIR
            args.ANALYSISDIR = os.path.join(
                os.path.dirname(infile), args.ANALYSISDIR)
            args.OUTPUTDIR = os.path.join(
                os.path.dirname(infile), args.OUTPUTDIR)

            ctdp = contur.factories.Depot(
                outputDir=args.OUTPUTDIR, noStack=args.NOSTACK)

            # get info from paramfile if it is there
            param_file_path = os.path.join(
                os.path.dirname(infile), contur.config.paramfile)
            if os.path.exists(param_file_path):
                params = contur.util.read_param_point(param_file_path)
                modeMessage += '\nSampled at:'
                for param, val in params.items():
                    modeMessage += '\n' + param + ': ' + str(val)
            else:
                params = {}
                params["No parameters specified"] = 0.0
                modeMessage += "\nParameter values not known for this run."


            if args.SLHA:            
                for param, val in params.items():
                    if param=="slha_file":
                        block_list = args.SLHA.split(",")
                        # read parameters from blocks in an SLHA file
                        block_dict = contur.util.read_slha_file(root, val, block_list)
                        for block in block_list:
                            params.update(block_dict[block])

            # If requested, grab some values from the log file and add them as extra parameters.
            root = "."
            files = os.listdir(root)

            # If requested, grab some values from the log file and add them as extra parameters.
            if args.ME:
                params.update(contur.util.read_out_file(
                    root, files, args.ME.split(",")))
            bf_list = None
            width_list = None
            mass_list = None
            if args.BF is not None:
                bf_list = args.BF.split(",")
            if args.WIDTH is not None:
                width_list = args.WIDTH.split(",")
            if args.MASS is not None:
                mass_list = args.MASS.split(",")

            if args.BF or args.WIDTH or args.MASS:
                params.update(contur.util.read_log_file(
                    root, files, bf_list, width_list, mass_list))

            # read the yodafile, do the comparison
            ctdp.add_point(param_dict=params, yodafile=infile)

            contur.config.contur_log.info(modeMessage)
            # contur.conturDepotInbox[0].yoda_factory)
            write_output(modeMessage, ctdp, args)

            args.ANALYSISDIR = anaDir  # reset ANALYSIDIR to original value
            args.OUTPUTDIR = outDir  # reset ANALYSIDIR to original value
