import pickle as pkl


def export(in_path, out_path, include_dominant_pools=False):
    with open(in_path, 'rb') as file:
        pkl.load(file).export(out_path, include_dominant_pools=include_dominant_pools)
