"""Global logger for contur with some features setup

:conturLog: (``logging.log``) -- Global python log, writeable with standard :mod:`logging` functionality

    * *Log Level* (``logging.ERROR``) --
      Default log level set to ERROR
    * *Log location* (``string``) --
      Default log location, contur.log
"""

import logging
import sys
class _DuplicateFilter(object):
    """Private class to filter log messages so only one instance shows once, from:
    from https://stackoverflow.com/questions/31953272/python-logging-print-message-only-once
    """
    def __init__(self):
        self.msgs = set()

    def filter(self, record):
        rv = record.msg not in self.msgs
        self.msgs.add(record.msg)
        return rv

dup_filter = _DuplicateFilter()
#for the stdout format lets just keep the message and levelname
FMT=logging.Formatter('%(levelname)s - %(message)s')
#conturLog = logging.getLogger()

#for some reason we can't supply a formatter to the basic config: https://stackoverflow.com/questions/34319521/python-logging-module-having-a-formatter-causes-attributeerror
logging.basicConfig(
    level=logging.ERROR,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    filemode="w",
    filename='contur.log',
    )

#format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

stream = logging.StreamHandler(sys.stdout)
stream.setFormatter(FMT)

contur_log=logging.getLogger()

contur_log.addHandler(stream)
contur_log.addFilter(dup_filter)
