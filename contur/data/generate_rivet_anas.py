import os
import rivet
import contur.data


def generate_rivet_anas(output_directory):
    # statics
    rivettxt = "insert Rivet:Analyses 0 "

    # make the directory if it doesn't already exist
    contur.scan.make_directory(output_directory)

    # get the lists of analyses
    aLists = contur.data.getAnalyses()

    # open file for the environment setters
    fl = open(output_directory + "/analysis-list", 'w')

    known_beams = contur.data.getBeams()
    envStrings = {}

    for beam, analysis_list in aLists.items():
        f = open(os.path.join(output_directory, beam + ".ana"), 'w')
        envStrings[beam] = "export CONTUR_RA" + beam + "=\""
        for analysis in analysis_list.split():
            ana = rivet.AnalysisLoader.getAnalysis(rivet.stripOptions(analysis))
            if ana is not None:
                f.write(rivettxt + analysis + " # " + ana.summary() + "\n")
                envStrings[beam] = envStrings.get(beam) + analysis + ","
            else:
                print("No analysis found for ", analysis)

    for estr in envStrings.values():
        estr = estr[:len(estr) - 1] + "\""
        fl.write(estr + "\n \n")

    # open file for the web page list
    data_list_file = open(output_directory + "/data-list.rst", 'w')
    data_list = "Current Data \n------------ \n"

    bvetoissue = "\nb-jet veto issue\n---------------- \n\n *The following measurements apply a detector-level b-jet veto which is not part of the particle-level fiducial definition and therefore not applied in Rivet. Also applies to CMS Higgs-to-WW analysis. Off by default, can be turned on via command-line, but use with care.* \n"

    higgsww = "\nHiggs to WW\n----------- \n\n *Typically involve large data-driven top background subtraction. If your model contributes to the background as well the results maybe unreliable. Off by default, can be turned on via command-line.* \n"

    higgsgg = "\nHiggs to diphotons\n------------------ \n\n *Higgs to two photons use a data-driven background subtraction. If your model predicts non-resonant photon production this may lead to unreliable results. On by default, can be turned off via command-line.* \n"

    ratios = "\nRatio measurements\n------------------ \n\n *These typically use SM theory for the denominator, and may give unreliable results if your model contributes to both numerator and denominator. On by default, can be turned off via command-line.* \n"

    searches = "\nSearches\n-------- \n\n *Detector-level, using Rivet smearing functions. Off by default, can be turned on via command-line.*\n"

    nutrue = "\nNeutrino Truth\n-------------- \n\n *Uses neutrino flavour truth info, may be misleading for BSM. Off by default, can be turned on via command-line.*\n"

    pools = contur.data.getPools()

    current_pool = ""

    for ana in sorted(pools, key=pools.get):

        if len(pools[ana]) > 0:
            pool_str = "\n Pool: **" + pools[ana] + "**  *" + \
                       contur.data.getDescription(pools[ana]) + "* \n\n"
            tmp_str = "   * `" + ana + " <https://rivet.hepforge.org/analyses/" + \
                      rivet.stripOptions(ana) + ".html>`_, "
            analysisObj = rivet.AnalysisLoader.getAnalysis(rivet.stripOptions(ana))
            if analysisObj is not None:
                tmp_str += analysisObj.summary() + " :cite:`" + analysisObj.bibKey() + "` \n"
            else:
                tmp_str += "Could not find this analysis in your Rivet install"
                print(
                    "Could not find %s in your Rivet install. Update Rivet, or add the analysis to the data/Rivet directory" % ana)

            if contur.data.hasRatio(ana):
                if pools[ana] in ratios:
                    ratios += tmp_str
                else:
                    ratios += pool_str + tmp_str

            elif contur.data.hasSearches(ana):
                if pools[ana] in searches:
                    searches += tmp_str
                else:
                    searches += pool_str + tmp_str

            elif contur.data.hasHiggsgg(ana):
                if pools[ana] in higgsgg:
                    higgsgg += tmp_str
                else:
                    higgsgg += pool_str + tmp_str

            elif contur.data.hasHiggsWW(ana):
                if pools[ana] in higgsww:
                    higgsww += tmp_str
                else:
                    higgsww += pool_str + tmp_str

            elif contur.data.hasNuTrue(ana):
                if pools[ana] in nutrue:
                    nutrue += tmp_str
                else:
                    nutrue += pool_str + tmp_str

            else:
                if pools[ana] in data_list:
                    data_list += tmp_str
                else:
                    data_list += pool_str + tmp_str

            if contur.data.hasBVeto(ana):
                if pools[ana] in bvetoissue:
                    bvetoissue += tmp_str
                else:
                    bvetoissue += pool_str + tmp_str


    data_list_file.write(data_list)
    data_list_file.write(ratios)
    data_list_file.write(higgsgg)
    data_list_file.write(searches)
    data_list_file.write(nutrue)
    data_list_file.write(higgsww)
    data_list_file.write(bvetoissue)

    print("Analysis and environment files written to %s" % output_directory)
